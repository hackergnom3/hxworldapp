package com.haxor.hxworld;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

public class CustomAdapter extends ArrayAdapter<String[]>{

    private final String[][] resource;

    public CustomAdapter(Context context, String[][] resource) {
        super(context, R.layout.custom_row, resource);
        this.resource = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)    {
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());

        View view = layoutInflater.inflate(R.layout.custom_row, parent, false);

        TextView titleView = (TextView) view.findViewById(R.id.title);

        titleView.setText(resource[position][0]);

        TextView bodyView = (TextView) view.findViewById(R.id.body);

        bodyView.setText(resource[position][1]);

        TextView authorView = (TextView) view.findViewById(R.id.author);

        authorView.setText("by " + resource[position][2]);

        return view;
    }
}
