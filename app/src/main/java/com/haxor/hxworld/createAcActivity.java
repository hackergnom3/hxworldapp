package com.haxor.hxworld;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;


public class createAcActivity extends ActionBarActivity {
    private static final String LOGIN_URL = "http://imgshr.webege.com/beta/doRegisterandroid.php";
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";
    JSONParser jsonParser = new JSONParser();
    private ProgressDialog pDialog;
    public final Pattern EMAIL_ADDRESS_PATTERN = Patterns.EMAIL_ADDRESS;
    private final Pattern PASSWORD_PATTERN = Pattern.compile("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\\\S+$).{4,}$");

    private Button bCreate;
    private EditText name;
    private EditText email;
    private EditText username;
    private EditText password;
    private EditText confirm_password;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_ac);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        name = (EditText) findViewById(R.id.createAc_name);
        email = (EditText) findViewById(R.id.createAc_email);
        username = (EditText) findViewById(R.id.createAc_username);
        password = (EditText) findViewById(R.id.createAc_password);
        confirm_password = (EditText) findViewById(R.id.createAc_confirmpassword);
        bCreate = (Button) findViewById(R.id.createAc_createbutton);

        bCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int err = 0;
                if(name.getText().toString().length() == 0) {
                    err++;
                    name.requestFocus();
                    name.setError("Name cannot be empty");
                }

                if(email.getText().toString().length() == 0)    {
                    err++;
                    email.requestFocus();
                    email.setError("Email cannot be empty");
                }

                if(username.getText().toString().length() == 0)    {
                    err++;
                    username.requestFocus();
                    username.setError("Username cannot be empty");
                }

                if(password.getText().toString().length() == 0)    {
                    err++;
                    password.requestFocus();
                    password.setError("Password cannot be empty");
                }

                if(!confirm_password.getText().toString().equals(password.getText().toString()) || confirm_password.getText().toString() == "")  {
                    err++;
                    confirm_password.requestFocus();
                    confirm_password.setError("Password does not match");
                }

                if(!checkEmail(email.getText().toString()))   {
                    err++;
                    email.requestFocus();
                    email.setError("Invalid email address");
                }

                if(password.getText().toString().length() < 8)    {
                    err++;
                    password.requestFocus();
                    password.setError("Password must be 8-32 characters long");
                }

                if(err++ == 0)   {
                    new AttemptCreateAc().execute();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_create_ac, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if(id == R.id.home) {
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean checkEmail(String email) {
        return EMAIL_ADDRESS_PATTERN.matcher(email).matches();
    }

    class AttemptCreateAc extends AsyncTask<String, String, String>  {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(createAcActivity.this);
            pDialog.setMessage("Attempting for login...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... args) {
            int success;
            String name = createAcActivity.this.name.getText().toString();
            String email = createAcActivity.this.email.getText().toString();
            String username = createAcActivity.this.username.getText().toString();
            String password = createAcActivity.this.password.getText().toString();


            try {
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("name", name));
                params.add(new BasicNameValuePair("email",email));
                params.add(new BasicNameValuePair("username", username));
                params.add(new BasicNameValuePair("password", password));
                JSONObject json = jsonParser.makeHttpRequest(LOGIN_URL, "POST", params);
                success = json.getInt(TAG_SUCCESS);
                if (success == 1) {
                    Log.d("Successfully Login!", json.toString());
                        Intent ii = new Intent(createAcActivity.this, login.class);
                        createAcActivity.this.finish();
                        startActivity(ii);
                    return json.getString(TAG_MESSAGE);
                }   else    {
                    return json.getString(TAG_MESSAGE);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String message) {
            {
                pDialog.dismiss();
                if (message != null) {
                    Toast.makeText(createAcActivity.this, message, Toast.LENGTH_LONG).show();
                }
            }
        }
    }
}
