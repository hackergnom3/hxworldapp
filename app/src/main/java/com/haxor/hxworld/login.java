package com.haxor.hxworld;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class login extends ActionBarActivity {
    private EditText user, pass;
    private Button bLogin;
    private ProgressDialog pDialog;
    private static final String LOGIN_URL = "http://imgshr.webege.com/beta/doLoginandroid.php";
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";
    JSONParser jsonParser = new JSONParser();
    SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        sharedpreferences = getSharedPreferences("session_login", Context.MODE_PRIVATE);

        user = (EditText) findViewById(R.id.login_username);
        pass = (EditText) findViewById(R.id.login_password);
        bLogin = (Button) findViewById(R.id.login_buttonlogin);
        bLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AttemptLogin().execute();
            }
        });

        TextView createAc = (TextView) findViewById(R.id.login_createAc);
        createAc.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent createAcIntent = new Intent(getApplicationContext(), createAcActivity.class);
                        startActivity(createAcIntent);
                    }
                }
        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.home) {
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    class AttemptLogin extends AsyncTask<String, String, String> {
        SharedPreferences.Editor editor = sharedpreferences.edit();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(login.this);
            pDialog.setMessage("Attempting for login...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... arg) {
            int success;
            String username = user.getText().toString();
            String password = pass.getText().toString();
            try {
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("username", username));
                params.add(new BasicNameValuePair("password", password));
                JSONObject json = jsonParser.makeHttpRequest(LOGIN_URL, "POST", params);
                success = json.getInt(TAG_SUCCESS);
                if (success == 1) {
                    Log.d("Successfully Login!", json.toString());
                        Intent ii = new Intent(login.this, MainActivity.class);
                        login.this.finish();
                        startActivity(ii);
                    editor.putBoolean("session_active", true);
                    editor.putString("session_username", user.getText().toString());
                    editor.commit();
                    return json.getString(TAG_MESSAGE);
                } else {
                    editor.putBoolean("session_active", false);
                    editor.commit();
                    return json.getString(TAG_MESSAGE);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            editor.putBoolean("session_active", false);
            editor.commit();
            return null;
        }

        protected void onPostExecute(String message) {
            {
                pDialog.dismiss();
                if (message != null) {
                    Toast.makeText(login.this, message, Toast.LENGTH_LONG).show();
                }
            }
        }
    }
}
