package com.haxor.hxworld;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends ActionBarActivity {

    ListView listView;
    CustomAdapter customAdapter;
    Context context = this;
    private ProgressDialog pDialog;
    SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sharedpreferences = getSharedPreferences("session_login", Context.MODE_PRIVATE);

        this.listView = (ListView) this.findViewById(R.id.listView);

        if(isInternetAvailable()) {
            new getPosts().execute(new ConnectorApi());
        }
        else    {
            //AlertDialog
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(R.string.networkunavailable)
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //Relaunch MainActivity
                            finish();
                            startActivity(getIntent());
                        }
                    })
                    .setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //Exit App/ Goto Home Launcher
                            Intent intent = new Intent(Intent.ACTION_MAIN);
                            intent.addCategory(Intent.CATEGORY_HOME);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                        }
                    })
                    .show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if(sharedpreferences.getBoolean("session_active", false))   {
            getMenuInflater().inflate(R.menu.menu_main_session, menu);
        }
        else {
            getMenuInflater().inflate(R.menu.menu_main, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.action_website)  {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.hxworld.tk"));
            startActivity(browserIntent);
        }

        if(id == R.id.action_refresh)   {
            finish();
            startActivity(getIntent());
        }

        if(id == R.id.action_login_activity)    {
            Intent loginIntent = new Intent(context, login.class);
            startActivity(loginIntent);
        }

        if(id == R.id.action_logout_activity)   {
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putBoolean("session_active", false);
            editor.remove("session_username");
            editor.commit();
            finish();
            startActivity(getIntent());
        }

        if(id == R.id.action_profile_activity)  {
            Intent profileIntent = new Intent(this, ProfileActivity.class);
            startActivity(profileIntent);
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    public void setTextToTextView(JSONArray jsonArray)  {
        String[][] strings = new String[jsonArray.length()][3];
        for(int i = 0; i < jsonArray.length(); i++) {

            JSONObject json = null;

            try {
                json = jsonArray.getJSONObject(i);

                strings[i][0] = json.getString("title");
                strings[i][1] = json.getString("body");
                strings[i][2] = json.getString("name");

            }catch (JSONException e)    {
                e.printStackTrace();
            }
        }

        customAdapter = new CustomAdapter(this, strings);

        this.listView.setAdapter(customAdapter);

    }

    private class getPosts extends AsyncTask<ConnectorApi,Long,JSONArray> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Loading posts");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected JSONArray doInBackground(ConnectorApi... connectorApis) {
            return connectorApis[0].getPostData();
        }

        @Override
        protected void onPostExecute(JSONArray jsonArray)   {
            pDialog.dismiss();
            setTextToTextView(jsonArray);
        }

    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    private boolean isInternetAvailable() {
        if(isNetworkAvailable()) {
            try {
                Process p1 = java.lang.Runtime.getRuntime().exec("ping -c 1 www.google.com");
                int returnVal = p1.waitFor();
                boolean reachable = (returnVal == 0);
                return reachable;
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return false;
        }
        return false;
    }

}